package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestConverter {

    static WebElement fromMeters;
    static WebElement fromKilometers;
    static WebElement fromMiles;
    static WebElement fromFeet;
    static WebElement fromYards;
    static WebElement toMeters;
    static WebElement toKilometers;
    static WebElement toMiles;
    static WebElement toFeet;
    static WebElement toYards;
    static WebElement formula;
    static WebElement enterField;
    static WebElement btConvert;
    static WebElement rez;
    public static ChromeDriver driver;



     public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/belichenko/converterLength/index.html");


        fromKilometers=driver.findElementByCssSelector("body > form > select > option:nth-child(2)");
        fromMeters=driver.findElementByCssSelector("body > form > select > option:nth-child(1)");
        fromMiles=driver.findElementByCssSelector("body > form > select > option:nth-child(3)");
        fromFeet=driver.findElementByCssSelector("body > form > select > option:nth-child(4)");
        fromYards=driver.findElementByCssSelector("body > form > select > option:nth-child(5)");
        toMeters=driver.findElementByCssSelector("body > form > p:nth-child(4) > select > option:nth-child(1)");
        toKilometers=driver.findElementByCssSelector("body > form > p:nth-child(4) > select > option:nth-child(2)");
        toMiles=driver.findElementByCssSelector("body > form > p:nth-child(4) > select > option:nth-child(3)");
        toFeet=driver.findElementByCssSelector("body > form > p:nth-child(4) > select > option:nth-child(4)");
        toYards=driver.findElementByCssSelector("body > form > p:nth-child(4) > select > option:nth-child(5)");
        formula=driver.findElementById("formula");
        enterField=driver.findElementByName("fromValue");
        btConvert=driver.findElementByCssSelector("body > form > p:nth-child(10) > input");
        rez=driver.findElementById("toValue");
        String num;
    }


    public String fromMetersToKilometers(String num){

        fromMeters.click();
        toKilometers.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;
    }
    public String fromMetersToMiles(String num){

        fromMeters.click();
        toMiles.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMetersToFeet(String num){

        fromMeters.click();
        toFeet.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMetersToYards(String num){

        fromMeters.click();
        toYards.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromKilometersToMeters(String num){

        fromKilometers.click();
        toMeters.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromKilometersToMiles(String num){

        fromKilometers.click();
        toMiles.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromKilometersToFeet(String num){

        fromKilometers.click();
        toFeet.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromKilometersToYards(String num){

        fromKilometers.click();
        toYards.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMilesToMeters(String num){

        fromMiles.click();
        toMeters.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMilesToKilometers(String num){

        fromMiles.click();
        toKilometers.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMilesToFeet(String num){

        fromMiles.click();
        toFeet.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromMilesToYards(String num){

        fromMiles.click();
        toYards.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromFeetToMeters(String num){

         fromFeet.click();
        toMeters.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromFeetToKilometers(String num){

        fromFeet.click();
        toKilometers.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromFeetToMiles(String num){

        fromFeet.click();
        toMiles.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromFeetToYards(String num){

        fromFeet.click();
        toYards.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromYardsToMeters(String num){

       fromYards.click();
        toMeters.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromYardsToKilometers(String num){

        fromYards.click();
        toKilometers.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromYardsToMiles(String num){

        fromYards.click();
        toMiles.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }
    public String fromYardsToFeet(String num){

        fromYards.click();
        toFeet.click();
        enterField.sendKeys(num);
        btConvert.click();
        String answer=rez.getText();
        return answer;

    }







}
