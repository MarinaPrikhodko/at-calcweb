package com.company;


import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.lang.*;
public class TestCalculator {

    static WebElement zero;
    static WebElement one;
    static WebElement two;
    static WebElement three;
    static WebElement four;
    static WebElement five;
    static WebElement six;
    static WebElement seven;
    static WebElement eight;
    static WebElement nine;
    static WebElement point;
    static WebElement plus;
    static WebElement minus;
    static WebElement divide;
    static WebElement multiply;
    static WebElement rez;
    static WebElement sin;
    static WebElement cos;
    static WebElement tan;
    static WebElement exp;
    static WebElement log;
    static WebElement log_2;
    static WebElement sqrt;
    static WebElement pow;
    static WebElement clearButton;
    static WebElement deleteButton;
    static WebElement negNum;
    static WebElement resultDisplay;
    static WebElement expressionDisplay;
    public static ChromeDriver driver;

    @BeforeAll
    public static void initDriver() {
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Downloads\\chromedriver_win32\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.get("http://34.90.36.71/apps/belichenko/scientificCalculator/index.html");

        zero=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[24]");
        one=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[20]");
        two=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[21]");
        three=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[22]");
        four=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[16]");
        five=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[17]");
        six=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[18]");
        seven=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[12]");
        eight=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[13]");
        nine=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[14]");
        point=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[25]");
        plus=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[15]");
        minus=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[19]");
        divide=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[27]");
        multiply=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[23]");
        rez=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[26]");
        sin=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[4]");
        cos=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[5]");
        tan=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[6]");
        exp=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[7]");
        log=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[8]");
        log_2=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[9]");
        pow=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[10]");
        sqrt=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[11]");
        negNum=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[3]");
        clearButton=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[1]");
        deleteButton=driver.findElementByXPath("/html/body/div/div[4]/div[3]/div[2]");
        expressionDisplay=driver.findElementByXPath("/html/body/div/div[4]/div[2]");
        resultDisplay= driver.findElementByXPath("/html/body/div/div[4]/div[1]");
}
     @AfterAll
     public static void quit(){
         driver.quit();
     }
     @AfterEach
     public void clearDisplay(){
      clearButton.click();

     }

     @Test
    public void testNumberButtons(){
        one.click();
        two.click();
        three.click();
        four.click();
        five.click();
        six.click();
        seven.click();
        eight.click();
        nine.click();
        zero.click();
        String exp="1234567890";
        String act = resultDisplay.getText();
        Assertions.assertEquals(exp,act);
   }
    @Test
    public void testClearButton(){
       one.click();
       two.click();
       three.click();
       four.click();
       five.click();
       six.click();
       seven.click();
       eight.click();
       nine.click();
       zero.click();
       clearButton.click();
       Assertions.assertEquals("0",resultDisplay.getText());
}
    @Test
    public void testDeleteButtonResultDisplay(){
    one.click();
    nine.click();
    multiply.click();
    five.click();
    zero.click();
    seven.click();
    deleteButton.click();
    Assertions.assertEquals("50",resultDisplay.getText());

  }
    @Test
    public void testDeleteButtonResultDisplay_2() {
        one.click();
        nine.click();
        multiply.click();
        five.click();
        zero.click();
        seven.click();
        for (int i = 1; i <= 5; i++) {
            deleteButton.click();
        }
        Assertions.assertEquals("0", resultDisplay.getText());

    }
    @Test
    public void testDeleteButtonExpressionDisplay(){
        one.click();
        nine.click();
        multiply.click();
        five.click();
        zero.click();
        seven.click();
        deleteButton.click();
        Assertions.assertEquals("19*50",expressionDisplay.getText());

    }
    @Test
    public void testDeleteButtonExpressionDisplay_2() {
        one.click();
        nine.click();
        multiply.click();
        five.click();
        zero.click();
        seven.click();
        for (int i = 1; i <= 5; i++) {
            deleteButton.click();
        }
        Assertions.assertEquals("1", expressionDisplay.getText());

    }
    @Test
    public void  testDefaultZerosExpressionDisplay() {
        for (int i = 1; i <= 4; i++) {
            zero.click();
        }
        one.click();
        seven.click();
        six.click();
        Assertions.assertEquals("176", expressionDisplay.getText());
    }
    @Test
    public void  testDefaultZerosResultDisplay(){
        for (int i=1;i<=4;i++){
            zero.click();
        }
        one.click();
        seven.click();
        six.click();
        Assertions.assertEquals("176", resultDisplay.getText());

    }
    @Test
    public void testDecimalNumberResultDisplay(){
    zero.click();
    point.click();
    for (int i=1;i<=4;i++){
            zero.click();
        }
    two.click();
        Assertions.assertEquals("0.00002", resultDisplay.getText());

    }
    @Test
    public void testDecimalNumberExpressionDisplay(){
        zero.click();
        point.click();
        for (int i=1;i<=4;i++){
            zero.click();
        }
        two.click();
        Assertions.assertEquals("0.00002", expressionDisplay.getText());

    }
    @Test
    public void testNegativeNumberResultDisplay(){
    five.click();
    negNum.click();
    Assertions.assertEquals("-5", resultDisplay.getText());

    }
    @Test
    public void testNegativeNumberExpressionDisplay(){
        five.click();
        negNum.click();
        Assertions.assertEquals("-5", expressionDisplay.getText());

    }
    @Test
    public void testEnterFieldExceptsOnly23Symbols(){
    for(int i=1;i<=50;i++){
        one.click();
    }
       Assertions.assertEquals("11111111111111111111111",resultDisplay.getText());


    }
    @Test
    public void testExponentialNotationOfResult(){
        for(int i=1;i<=15;i++){
            five.click();
        }
        multiply.click();
        one.click();
        for(int i=1;i<=14;i++){
            zero.click();
        }
        rez.click();

        Assertions.assertEquals("5.55555555555555e+28",resultDisplay.getText());


    }
    @Test
    public void testSumOfTwoPositiveNumbers(){
        two.click();
        five.click();
        plus.click();
        six.click();
        nine.click();
        rez.click();
        Assertions.assertEquals("94",resultDisplay.getText());

    }
    @Test
    public void testSumOfPositiveAndNegativeNumbers(){
        two.click();
        five.click();
        zero.click();
        plus.click();
        six.click();
        nine.click();
        four.click();
        three.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("-6693",resultDisplay.getText());
    }
    @Test
    public void testSumOfNegativeAndPositiveNumbers(){
        six.click();
        nine.click();
        four.click();
        three.click();
        negNum.click();
        plus.click();
        two.click();
        five.click();
        zero.click();
        rez.click();
        Assertions.assertEquals("-6693",resultDisplay.getText());
    }
    @Test
    public void testSumOfTwoNegativeNumbers(){
        six.click();
        nine.click();
        four.click();
        three.click();
        negNum.click();
        plus.click();
        two.click();
        five.click();
        zero.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("-7193",resultDisplay.getText());
    }
    @Test
    public void testSumOfTwoDecimalNumbers(){
       zero.click();
       point.click();
       zero.click();
       one.click();
       four.click();
       nine.click();
       plus.click();
       two.click();
       seven.click();
       point.click();
       eight.click();
       three.click();
        rez.click();
        Assertions.assertEquals("27.8449",resultDisplay.getText());
    }
    @Test
    public void testSubtractionOfTwoPositiveNumbers(){
        two.click();
        five.click();
        seven.click();
        minus.click();
        one.click();
        nine.click();
        rez.click();
        Assertions.assertEquals("238",resultDisplay.getText());

    }
    @Test
    public void testSubtractionOfPositiveAndNegativeNumbers(){
        five.click();
        zero.click();
        minus.click();
        four.click();
        three.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("93",resultDisplay.getText());
    }
    @Test
    public void testSubtractionOfNegativeAndPositiveNumbers(){
        nine.click();
        four.click();
        three.click();
        negNum.click();
        minus.click();
        two.click();
        five.click();
        zero.click();
        six.click();
        eight.click();
        rez.click();
        Assertions.assertEquals("-26011",resultDisplay.getText());
    }
    @Test
    public void testSubtractionOfTwoNegativeNumbers(){

        nine.click();
        three.click();
        negNum.click();
        minus.click();
        eight.click();
        five.click();
        zero.click();
        zero.click();
        nine.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("84916",resultDisplay.getText());
    }
    @Test
    public void testSubtractionOfTwoDecimalNumbers(){
        zero.click();
        point.click();
        for(int i=1;i<=5;i++){
            one.click();
        }
        for(int i=1;i<=4;i++){
            five.click();
        }
        minus.click();
        zero.click();
        point.click();
        for(int i=1;i<=9;i++){
            zero.click();
        }
        two.click();
        rez.click();

        Assertions.assertEquals("0.1111155548",resultDisplay.getText());
    }
    @Test
    public void testMultiplyOfTwoPositiveNumbers(){
        one.click();
        for (int i=1; i<=5;i++){
            nine.click();
        }
        multiply.click();
        for(int i=1;i<=5;i++){
            nine.click();
        }
        rez.click();
        Assertions.assertEquals("19999700001",resultDisplay.getText());

    }
    @Test
    public void testMultiplyOfPositiveAndNegativeNumbers(){
        five.click();
        zero.click();
        multiply.click();
        four.click();
        three.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("-1075",resultDisplay.getText());
    }
    @Test
    public void testMultiplyOfNegativeAndPositiveNumbers(){
        nine.click();
        four.click();
        three.click();
        negNum.click();
        multiply.click();
        two.click();
        five.click();
        zero.click();
        six.click();
        eight.click();
        rez.click();
        Assertions.assertEquals("-23639124",resultDisplay.getText());
    }
    @Test
    public void testMultiplyOfTwoNegativeNumbers(){

        nine.click();
        three.click();
        negNum.click();
        multiply.click();
        eight.click();
        five.click();
        zero.click();
        zero.click();
        nine.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("7905837",resultDisplay.getText());
    }
    @Test
    public void testMultiplyOfTwoDecimalNumbers(){
        zero.click();
        point.click();
        one.click();
        five.click();
        multiply.click();
        two.click();
        point.click();
        three.click();
        five.click();
        rez.click();
        Assertions.assertEquals("0.3525",resultDisplay.getText());
    }
    @Test
    public void testDivideOfTwoPositiveNumbers(){
       for(int i=1;i<=5;i++){
           nine.click();
       }
       divide.click();
       for(int i=1;i<=4;i++){
           two.click();
       }
       rez.click();
        Assertions.assertEquals("45.0040504050405",resultDisplay.getText());

    }
    @Test
    public void testDivideOfPositiveAndNegativeNumbers(){
        five.click();
        zero.click();
        divide.click();
        four.click();
        three.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("-1.16279006977",resultDisplay.getText());
    }
    @Test
    public void testDivideOfNegativeAndPositiveNumbers(){
     two.click();
     four.click();
     zero.click();
     zero.click();
     negNum.click();
     divide.click();
     one.click();
     two.click();
     zero.click();
     zero.click();
     rez.click();
        Assertions.assertEquals("-2",resultDisplay.getText());
    }
    @Test
    public void testDivideOfTwoNegativeNumbers(){

        nine.click();
        negNum.click();
        divide.click();
        three.click();
        negNum.click();
        rez.click();
        Assertions.assertEquals("3",resultDisplay.getText());
    }
    @Test
    public void testDivideOfTwoDecimalNumbers(){
        zero.click();
        point.click();
        for(int i=1;i<=3;i++){
            zero.click();
        }
        five.click();
        divide.click();
        zero.click();
        point.click();
        for(int i=1;i<=3;i++) {
            zero.click();
        }
        two.click();
        rez.click();
        Assertions.assertEquals("2.5",resultDisplay.getText());
    }
    @Test
    public void testDivideOnZero(){
        six.click();
        divide.click();
        zero.click();
        rez.click();
        Assertions.assertEquals("ERROR",resultDisplay.getText());
    }

    @Test
    public void testSin_5(){
        five.click();
        sin.click();
        Assertions.assertEquals("0.087155742748",resultDisplay.getText().substring(0,14));


    }
    @Test
    public void testSin_61(){
        six.click();
        one.click();
        sin.click();
        Assertions.assertEquals("0.874619707139",resultDisplay.getText().substring(0,14));

    }
    @Test
    public void testSin_128(){
        one.click();
        two.click();
        eight.click();
        sin.click();
        Assertions.assertEquals("0.788010753606",resultDisplay.getText().substring(0,14));

    }
    @Test
    public void testSin_180(){
        one.click();
        eight.click();
        zero.click();
        sin.click();
        Assertions.assertEquals("0",resultDisplay.getText());

    }
    @Test
    public void testSin_30(){
        three.click();
        zero.click();
        sin.click();
        Assertions.assertEquals("0.5",resultDisplay.getText());
    }
    @Test
    public void testSin_neg351(){
        three.click();
        five.click();
        one.click();
        negNum.click();
        sin.click();
        Assertions.assertEquals("0.15643446504",resultDisplay.getText().substring(0,13));
    }
    @Test
    public void testSin_neg61(){
        six.click();
        one.click();
        negNum.click();
        sin.click();
        Assertions.assertEquals("-0.874619707139",resultDisplay.getText().substring(0,15));
    }
    @Test
    public void testSinDecimal(){
        zero.click();
        point.click();
        one.click();
        sin.click();
        Assertions.assertEquals("0.001745328365",resultDisplay.getText().substring(0,14));
    }
    @Test
    public void testCos_5(){
        five.click();
        cos.click();
        Assertions.assertEquals("0.996194698092",resultDisplay.getText().substring(0,14));


    }
    @Test
    public void testCos_61(){
        six.click();
        one.click();
        cos.click();
        Assertions.assertEquals("0.484809620246",resultDisplay.getText().substring(0,14));

    }
    @Test
    public void testCos_128(){
        one.click();
        two.click();
        eight.click();
        cos.click();
        Assertions.assertEquals("-0.615661475326",resultDisplay.getText().substring(0,15));

    }
    @Test
    public void testCos_180(){
        one.click();
        eight.click();
        zero.click();
        cos.click();
        Assertions.assertEquals("-1",resultDisplay.getText());

    }
    @Test
    public void testCos_30(){
        three.click();
        zero.click();
        cos.click();
        Assertions.assertEquals("0.866025403784",resultDisplay.getText().substring(0,14));
    }
    @Test
    public void testCos_neg351(){
        three.click();
        five.click();
        one.click();
        negNum.click();
        cos.click();
        Assertions.assertEquals("0.987688340595",resultDisplay.getText().substring(0,14));
    }
    @Test
    public void testCos_neg61(){
        six.click();
        one.click();
        negNum.click();
        cos.click();
        Assertions.assertEquals("0.484809620246",resultDisplay.getText().substring(0,14));
    }
    @Test
    public void testCosDecimal(){
        zero.click();
        point.click();
        one.click();
        cos.click();
        Assertions.assertEquals("0.999998476913",resultDisplay.getText().substring(0,14));
    }
    @Test
    public void testTan_5(){
    five.click();
    tan.click();
    Assertions.assertEquals("0.087488663526",resultDisplay.getText());


}
    @Test
    public void testTan_61(){
        six.click();
        one.click();
        tan.click();
        Assertions.assertEquals("1.8040477552716",resultDisplay.getText());

    }
    @Test
    public void testTan_128(){
        one.click();
        two.click();
        eight.click();
        tan.click();
        Assertions.assertEquals("-1.279941632193",resultDisplay.getText());

    }
    @Test
    public void testTan_180(){
        one.click();
        eight.click();
        zero.click();
        tan.click();
        Assertions.assertEquals("0",resultDisplay.getText());

    }
    @Test
    public void testTan_30(){
        three.click();
        zero.click();
        tan.click();
        Assertions.assertEquals("0.57735026919",resultDisplay.getText());
    }
    @Test
    public void testTan_neg351(){
        three.click();
        five.click();
        one.click();
        negNum.click();
        tan.click();
        Assertions.assertEquals("0.158384440325",resultDisplay.getText());
    }
    @Test
    public void testTan_neg61(){
        six.click();
        one.click();
        negNum.click();
        tan.click();
        Assertions.assertEquals("-1.804047755271",resultDisplay.getText().substring(0,1));
    }
    @Test
    public void testTanDecimal(){
        zero.click();
        point.click();
        one.click();
        tan.click();
        Assertions.assertEquals("0.001745331024",resultDisplay.getText());
    }
    @Test
    public void testExp1(){
        one.click();
        exp.click();
        Assertions.assertEquals("2.718281828459045",resultDisplay.getText());
    }
    @Test
    public void testExp0(){
        zero.click();
        exp.click();
        Assertions.assertEquals("0",resultDisplay.getText());
    }
    @Test
    public void testExp2() {
        two.click();
        exp.click();
        Assertions.assertEquals("5.4365636569180905", resultDisplay.getText());
    }
    @Test
    public void testExp10() {
        one.click();
        zero.click();
        exp.click();
        Assertions.assertEquals("27.1828182845904524", resultDisplay.getText());
    }
    @Test
    public void testExpNeg1(){
        one.click();
        exp.click();
        Assertions.assertEquals("-2.718281828459045",resultDisplay.getText());
    }
    @Test
    public void testLogNeg1(){
        one.click();
        negNum.click();
        log.click();
        Assertions.assertEquals("You broke it!",resultDisplay.getText());

    }
    @Test
    public void testLog0(){
        zero.click();
        log.click();
        Assertions.assertEquals("Infinity",resultDisplay.getText());

    }
    @Test
    public void testLog1(){
        one.click();
        log.click();
        Assertions.assertEquals("0",resultDisplay.getText());

    }
    @Test
    public void testLog9(){
        nine.click();
        log.click();
        Assertions.assertEquals("0.9542425094393249",resultDisplay.getText());

    }
    @Test
    public void testLog19(){
        one.click();
        nine.click();
        log.click();
        Assertions.assertEquals("1.2787536009528289",resultDisplay.getText());

    }
    @Test
    public void testLog99(){
        nine.click();
        nine.click();
        log.click();
        Assertions.assertEquals("1.99563519459755",resultDisplay.getText());

    }
    @Test
    public void testLog100(){
        one.click();
        zero.click();
        zero.click();
        log.click();
        Assertions.assertEquals("2",resultDisplay.getText());

    }
    @Test
    public void testLog101(){
        one.click();
        zero.click();
        one.click();
        log.click();
        Assertions.assertEquals("2.0043213737826426",resultDisplay.getText());

    }
    @Test
    public void testLog999(){
        nine.click();
        nine.click();
        nine.click();
        log.click();
        Assertions.assertEquals("2.9995654882259823",resultDisplay.getText());

    }
    @Test
    public void testLog1001(){
        one.click();
        zero.click();
        zero.click();
        one.click();
        log.click();

        Assertions.assertEquals("3.000434077479319",resultDisplay.getText());

    }
    @Test
    public void testLog2Neg1(){
        one.click();
        negNum.click();
        log_2.click();
        Assertions.assertEquals("You broke it!",resultDisplay.getText());

    }
    @Test
    public void testLog2_0(){
        zero.click();
        log_2.click();
        Assertions.assertEquals("Infinity",resultDisplay.getText());

    }
    @Test
    public void testLog2_1(){
        one.click();
        log.click();
        Assertions.assertEquals("0",resultDisplay.getText());

    }
    @Test
    public void testLog2_9(){
        nine.click();
        log_2.click();
        Assertions.assertEquals("0.9542425094393249",resultDisplay.getText());

    }
    @Test
    public void testLog2_19(){
        one.click();
        nine.click();
        log_2.click();
        Assertions.assertEquals("1.2787536009528289",resultDisplay.getText());

    }
    @Test
    public void testLog2_99(){
        nine.click();
        nine.click();
        log_2.click();
        Assertions.assertEquals("1.99563519459755",resultDisplay.getText());

    }
    @Test
    public void testLog2_100(){
        one.click();
        zero.click();
        zero.click();
        log_2.click();
        Assertions.assertEquals("2",resultDisplay.getText());

    }
    @Test
    public void testLog2_101(){
        one.click();
        zero.click();
        one.click();
        log.click();
        Assertions.assertEquals("2.0043213737826426",resultDisplay.getText());

    }
    @Test
    public void testLog2_999(){
        nine.click();
        nine.click();
        nine.click();
        log_2.click();
        Assertions.assertEquals("2.9995654882259823",resultDisplay.getText());

    }
    @Test
    public void testLog2_1001(){
        one.click();
        zero.click();
        zero.click();
        one.click();
        log_2.click();

        Assertions.assertEquals("3.000434077479319",resultDisplay.getText());

    }
    @Test
    public void testPow0(){
        zero.click();
        pow.click();
        Assertions.assertEquals("0",resultDisplay.getText());

    }
    @Test
    public void testPowNeg1(){
        one.click();
        negNum.click();
        pow.click();

        Assertions.assertEquals("1",resultDisplay.getText());

    }
    @Test
    public void testPow1(){
        one.click();
        pow.click();

        Assertions.assertEquals("1",resultDisplay.getText());

    }
    @Test
    public void testPow9(){
        nine.click();
        pow.click();

        Assertions.assertEquals("81",resultDisplay.getText());

    }
    @Test
    public void testPow19(){
        one.click();
        nine.click();
        pow.click();

        Assertions.assertEquals("361",resultDisplay.getText());

    }
    @Test
    public void testPow99(){
        nine.click();
        nine.click();
        pow.click();

        Assertions.assertEquals("9801",resultDisplay.getText());

    }
    @Test
    public void testPow100(){
        one.click();
        zero.click();
        zero.click();
        pow.click();

        Assertions.assertEquals("10000",resultDisplay.getText());

    }
    @Test
    public void testPow101(){
        one.click();
        zero.click();
        one.click();
        pow.click();

        Assertions.assertEquals("10201",resultDisplay.getText());

    }
    @Test
    public void testPow999(){
        nine.click();
        nine.click();
        nine.click();
        pow.click();

        Assertions.assertEquals("998001",resultDisplay.getText());

    }
    @Test
    public void testPow1000(){
        one.click();
        zero.click();
        zero.click();
        zero.click();
        pow.click();

        Assertions.assertEquals("1000000",resultDisplay.getText());

    }
    @Test
    public void testPow1001(){
        one.click();
        zero.click();
        zero.click();
        one.click();
        pow.click();


        Assertions.assertEquals("1002001",resultDisplay.getText());

    }
    @Test
    public void testPow9999(){
        nine.click();
        nine.click();
        nine.click();
        nine.click();
        pow.click();

        Assertions.assertEquals("99980001",resultDisplay.getText());

    }
    @Test
    public void testSqrt0(){
    zero.click();
    sqrt.click();
    Assertions.assertEquals("0",resultDisplay.getText());

}
    @Test
    public void testSqrtNeg1(){
        one.click();
        negNum.click();
        sqrt.click();

        Assertions.assertEquals("NaN",resultDisplay.getText());

    }
    @Test
    public void testSqrt1(){
        one.click();
        sqrt.click();

        Assertions.assertEquals("1",resultDisplay.getText());

    }
    @Test
    public void testSqrt9(){
        nine.click();
        sqrt.click();

        Assertions.assertEquals("3",resultDisplay.getText());

    }
    @Test
    public void testSqrt19(){
        one.click();
        nine.click();
        sqrt.click();

        Assertions.assertEquals("4.3588989435",resultDisplay.getText().substring(0,12));

    }
    @Test
    public void testSqrt99(){
        nine.click();
        nine.click();
        sqrt.click();

        Assertions.assertEquals("9.949874371",resultDisplay.getText().substring(0,11));

    }
    @Test
    public void testSqrt00(){
        one.click();
        zero.click();
        zero.click();
        sqrt.click();

        Assertions.assertEquals("10",resultDisplay.getText());

    }
    @Test
    public void testSqrt101(){
        one.click();
        zero.click();
        one.click();
        sqrt.click();

        Assertions.assertEquals("10.0498756211",resultDisplay.getText().substring(0,13));

    }
    @Test
    public void testSqrt999(){
        nine.click();
        nine.click();
        nine.click();
        sqrt.click();

        Assertions.assertEquals("31.6069612585",resultDisplay.getText().substring(0,13));

    }
    @Test
    public void testSqrt1000(){
        one.click();
        zero.click();
        zero.click();
        zero.click();
        sqrt.click();

        Assertions.assertEquals("31.6227766016",resultDisplay.getText().substring(0,13));

    }
    @Test
    public void testSqrt1001(){
        one.click();
        zero.click();
        zero.click();
        one.click();
        sqrt.click();


        Assertions.assertEquals("31.6385840391",resultDisplay.getText().substring(0,13));

    }
    @Test
    public void testSqrt9999(){
        nine.click();
        nine.click();
        nine.click();
        nine.click();
        sqrt.click();

        Assertions.assertEquals("99.99499987",resultDisplay.getText().substring(0,11));

    }
    @Test
    public void testOrderOfPerformOperations(){
        one.click();
        plus.click();
        two.click();
        minus.click();
        three.click();
        divide.click();
        four.click();
        multiply.click();
        five.click();
        rez.click();

        Assertions.assertEquals("-0.75",resultDisplay.getText());
    }














}
