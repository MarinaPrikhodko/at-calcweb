package com.company;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class Tests extends TestConverter{



    @BeforeAll
    public static void start(){
        TestConverter.initDriver();
    }
    @BeforeEach
    public void clear1(){
        TestConverter.enterField.clear();
    }

    @AfterEach
    public void clear(){
        TestConverter.enterField.clear();
    }
    @AfterAll
    public static void quit(){
        TestConverter.driver.quit();
    }

    @ParameterizedTest
    @CsvSource(value = {"0.0001:0.1","0.00001:0.01","0.000001:0.001","0.001:1","0:0","-0.001:-1","0.009:9","0.01:10","0.011:11","0.019:19","0.02:20","0.021:21","0.099:99","0.1:100",
            "0.101:101","0.999:999","1:1000", "1.001:1001","9.999:9999","10:10000","10.001:10001",
            "99.999:99999","100:100000","100.001:100001","999.999:999999","1000:1000000","1000.001:1000001",
            "9999.999:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')
    void test_fromMetersToKilometers(String exp,String num){

        Assertions.assertEquals(exp,fromMetersToKilometers(num));

    }

    @ParameterizedTest
    @CsvSource(value = {"0.0000621:0.1","0.0000062:0.01","6.210000000000001e-7:0.001","0.000621:1","0:0","-0.000621:-1",
            "0.005592:9","0.00621:10","0.006835:11","0.011806:19","0.01242:20","0.013041:21","0.061479:99","0.0621:100",
            "0.062721:101","0.620379:999","0.621:1000", "0.621621:1001","6.209379:9999","6.21:10000","6.210621:10001",
            "62.099379:99999","62.1:100000","62.100621:100001","620.999379:999999","621:1000000","621.000621:1000001",
            "6209.999379:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromMetersToMiles(String exp,String num){

        Assertions.assertEquals(exp,fromMetersToMiles(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.328084:0.1","0.0328084:0.01","0.00328084:0.001","3.28084:1","0:0","-3.28084:-1","29.52756:9","32.8084:10",
            "36.08924:11","62.33596:19","65.6168:20","68.89764:21","324.80316:99","328.084:100",
            "331.36484:101","3277.55916:999","3280.84:1000", "3284.12084:1001","32805.11916:9999","32808.4:10000","32811.68084:10001",
            "328080.71916:99999","328084:100000","328087.28084:100001","3280836.71916:999999","3280840:1000000",
            "3280843.28084:1000001", "32808396.71916:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromMetersToFeet(String exp,String num){

        Assertions.assertEquals(exp,fromMetersToFeet(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.109361:0.1","0.0109361:0.01","0.00109361:0.001","1.09361:1","0:0","-1.09361:-1","9.84249:9","10.9361:10",
            "12.02971:11","20.77859:19","21.8722:20","22.96581:21","108.26739:99","109.361:100",
            "110.45461:101","1092.51639:999","1093.61:1000", "1094.70361:1001","10935.00639:9999","10936.1:10000",
            "10937.19361:10001",
            "109359.90639:99999","109361:100000","109362.09361:100001","1093608.90639:999999","1093610:1000000","1093611.09631:1000001",
            "10936098.90639:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromMetersToYards(String exp,String num){

        Assertions.assertEquals(exp,fromMetersToYards(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"100:0.1","10:0.01","1:0.001","1000:1","0:0","-1000:-1","9000:9","10000:10",
            "11000:11","19000:19","20000:20","21000:21","99000:99","100000:100",
            "101000:101","999000:999","1000000:1000", "1001000:1001","9999000:9999","10000000:10000","10001000:10001",
            "99999000:99999","100000000:100000","100001000:100001","999999000:999999","1000000000:1000000",
            "1000001000:1000001",
            "9999999000:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromKilometersToMeters(String exp,String num){

        Assertions.assertEquals(exp,fromKilometersToMeters(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.0621:0.1","0.00621:0.01","0.000621:0.001","0.621:1","0:0","-0.621:-1","5.589:9","6.21:10","6.831:11",
            "11.799:19","12.42:20","13.041:21","61.479:99","62.1:100",
            "62.721:101","620.379:999","621:1000", "621.621:1001","6209.379:9999","6210:10000","6210.621:10001",
            "62099.379:99999","62100:100000","62100.621:100001","620999.379:999999","621000:1000000","621000.621:1000001",
            "6209999.379:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromKilometersToMiles(String exp,String num){

        Assertions.assertEquals(exp,fromKilometersToMiles(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"328.084:0.1","32.8084:0.01","3.28084:0.001","3280.84:1","0:0","-3280.84:-1",
            "29527.56:9","32808.4:10","36089.24:11","62335.96:19","65616.8:20","68897.64:21","324803.16:99","328084:100",
            "331364.84:101","3277559.16:999","3280840:1000", "3284120.84:1001","32805119.16:9999","32808400:10000","32811680.84:10001",
            "328080719.15:99999","328084000:100000","328087280.84:100001","3280836719.16:999999","3280840000:1000000",
            "3280843280.84:1000001",
            "32808396719.16:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromKilometersToFeet(String exp,String num){

        Assertions.assertEquals(exp,fromKilometersToFeet(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"109.361:0.1","10.9361:0.01","1.09361:0.001","1093.61:1","0:0","-1093.61:-1",
            "9842.49:9","10936.1:10","12029.71:11","20778.59:19","21872.2:20","22965.81:21","108267.39:99","109361:100",
            "110454.61:101","1092516.39:999","1093610:1000", "1094703.61:1001","10935006.39:9999","10936100:10000","10937193.61:10001",
            "109359906.39:99999","109361000:100000","109362093.61:100001","1093608906.39:999999","1093610000:1000000",
            "1093611093.61:1000001",
            "10936098906.39:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromKilometersToYards(String exp,String num){

        Assertions.assertEquals(exp,fromKilometersToYards(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"160.934:0.1","16.0934:0.01","1.60934:0.001","1609.34:1","0:0","-1609.34:-1","14484.06:9",
            "16093.4:10","17702.74:11", "30577.46:19","32186.8:20","33796.14:21","159324.66:99","160934:100",
            "162543.34:101","1607730.66:999","1609340:1000", "1610949.34:1001","16091790.66:9999","16093400:10000",
            "16095009.34:10001", "160932390.66:99999","160934000:100000","160935609.34:100001","1609338390.66:999999",
            "1609340000:1000000", "1609341609.34:1000001", "16093398390.66:9999999", "Not a valid number.:qwerty",
            "Not a valid number.:!","Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"},
            delimiter = ':')

    void test_fromMilesToMeters(String exp,String num){

        Assertions.assertEquals(exp,fromMilesToMeters(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.160934:0.1","0.0160934:0.01","0.00160934:0.001","1.60934:1","0:0","-1.60934:-1","14.48406:9",
            "16.0934:10","17.70274:11", "30.57746:19","32.1868:20","33.79614:21","159.32466:99","160.934:100",
            "162.54334:101","1607.73066:999","1609.34:1000", "1610.94934:1001","16091.79066:9999","16093.4:10000",
            "16095.00934:10001", "160932.39066:99999","160934:100000","160935.60934:100001","1609338.39066:999999",
            "1609340:1000000", "1609341.60934:1000001", "16093398.39066:9999999", "Not a valid number.:qwerty",
            "Not a valid number.:!","Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"},
            delimiter = ':')

    void test_fromMilesToKilometers(String exp,String num){

        Assertions.assertEquals(exp,fromMilesToKilometers(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"528:0.1","52.8:0.01","5.28:0.001","5280:1","0:0","-5280:-1","47520:9","52800:10","58080:11",
            "100320:19", "105600:20","110880:21","522720:99","528000:100", "533280:101","5274720:999","5280000:1000",
            "5285280:1001","52794720:9999","52800000:10000", "52805280:10001", "527994720:99999","528000000:100000",
            "528005280:100001","5279994720:999999","5280000000:1000000","5280005280:1000001", "52799994720:9999999",
            "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+", "Not a valid number.:-",
            "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromMilesToFeet(String exp,String num){

        Assertions.assertEquals(exp,fromMilesToFeet(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"176:0.1","17.6:0.01","1.76:0.001","1760:1","0:0","-1760:-1","15840:9","17600:10","19360:11",
            "33440:19", "35200:20","36960:21","174240:99","176000:100", "177760:101","1758240:999","1760000:1000",
            "1761760:1001","17598240:9999","17600000:10000","17601760:10001", "175998240:99999","176000000:100000",
            "176001760:100001","1759998240:999999","1760000000:1000000","1760001760:1000001", "17599998240:9999999",
            "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+", "Not a valid number.:-",
            "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromMilesToYards(String exp,String num){

        Assertions.assertEquals(exp,fromMilesToYards(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.03048:0.1","0.003048:0.01","0.0003048:0.001","0.3048:1","0:0","-0.3048:-1","2.7432:9",
            "3.048:10","3.3528:11","5.7912:19","6.096:20","6.4008:21","30.1752:99","30.48:100", "30.7848:101",
            "304.4952:999","304.8:1000", "305.1048:1001","3047.6952:9999","3048:10000","3048.3048:10001",
            "30479.6952:99999","30480:100000","30480.3048:100001","304799.6952:999999","304800:1000000",
            "304800.3048:1000001", "3047999.6952:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromFeetToMeters(String exp,String num){

        Assertions.assertEquals(exp,fromFeetToMeters(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.00003048:0.1","0.000003048:0.01","0.0000003048:0.001","0.0003048:1","0:0","-0.0003048:-1",
            "0.0027432:9", "0.003048:10","0.0033528:11","0.0057912:19","0.006096:20","0.0064008:21","0.0301752:99",
            "0.03048:100", "0.0307848:101", "0.3044952:999","0.3048:1000", "0.3051048:1001","3.0476952:9999",
            "3.048:10000","3.0483048:10001", "30.4796952:99999","30.48:100000","30.4803048:100001",
            "304.7996952:999999","304.8:1000000", "304.8003048:1000001", "3047.9996952:9999999",
            "Not a valid number.:qwerty","Not a valid number.:!", "Not a valid number.:+", "Not a valid number.:-",
            "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromFeetToKilometers(String exp,String num){

        Assertions.assertEquals(exp,fromFeetToKilometers(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.0000189394:0.1","0.00000189394:0.01","0.000000189394:0.001"," 0.000189394:1","0:0",
            "-0.000189394:-1","0.001704546:9","0.00189394:10","0.002083334:11","0.003598486:19","0.00378788:20",
            "0.003977274:21","0.018750006:99","0.0189394:100", "0.019128794:101","0.189204606:999","0.189394:1000",
            "0.189583394:1001","1.893750606:9999","1.89394:10000","1.894129394:10001", "18.939210606:99999",
            "18.9394:100000","18.939589394:100001","189.393810606:999999","189.394:1000000","189.394189394:1000001",
            "1893.939810606:9999999", "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromFeetToMiles(String exp,String num){

        Assertions.assertEquals(exp,fromFeetToMiles(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.0333333:0.1","0.00333333:0.01","0.000333333:0.001","0.333333:1","0:0","-0.333333:-1",
            "2.999997:9","3.33333:10","3.666663:11","6.333327:19","6.66666:20","6.999993:21","32.999967:99",
            "33.3333:100", "33.666633:101","332.999667:999","333.333:1000", "333.666333:1001","3332.996667:9999",
            "3333.33:10000","3333.663333:10001", "33332.966667:99999","33333.3:100000","33333.633333:100001",
            "333332.666667:999999","333333:1000000","333333.333333:1000001", "3333329.666667:9999999",
            "Not a valid number.:qwerty","Not a valid number.:!","Not a valid number.:+",
            "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromFeetToYards(String exp,String num){

        Assertions.assertEquals(exp,fromFeetToYards(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.09144:0.1","0.009144:0.01","0.0009144:0.001","0.9144:1","0:0","-0.9144:-1","8.2296:9",
            "9.144:10","10.0584:11","17.3736:19","18.288:20","19.2024:21","90.5256:99","91.44:100",
            "92.3544:101","913.4856:999","914.4:1000", "915.3144:1001","9143.0856:9999","9144:10000","9144.9144:10001",
            "91439.0856:99999","91440:100000","91440.9144:100001","914399.0856:999999","914400:1000000",
            "914400.9144:1000001", "9143999.0856:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromYardsToMeters(String exp,String num){

        Assertions.assertEquals(exp,fromYardsToMeters(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.00009144:0.1","0.000009144:0.01","0.0000009144:0.001","0.0009144:1","0:0","-0.0009144:-1","0.0082296:9",
            "0.009144:10","0.0100584:11","0.0173736:19","0.018288:20","0.0192024:21","0.0905256:99","0.09144:100",
            "0.0923544:101","0.9134856:999","0.9144:1000", "0.9153144:1001","9.1430856:9999","9.144:10000","9.1449144:10001",
            "91.4390856:99999","91.44:100000","91.4409144:100001","914.3990856:999999","914.4:1000000",
            "914.4009144:1000001", "9143.9990856:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromYardsToKilometers(String exp,String num){

        Assertions.assertEquals(exp,fromYardsToKilometers(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.0000568182:0.1","0.00000568182:0.01","0.000000568182:0.001","0.000568182:1","0:0",
            "-0.000568182:-1", "0.005113638:9","0.00568182:10","0.006250002:11","0.010795458:19","0.01136364:20",
            "0.011931822:21","0.056250018:99","0.0568182:100", "0.057386382:101","0.567613818:999",
            "0.568182:1000", "0.568750182:1001","5.681251818:9999","5.68182:10000","5.682388182:10001",
            "56.817631818:99999","56.8182:100000","56.818768182:100001","568.181431818:999999","568.182:1000000",
            "568.182568182:1000001", "5681.819431818:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromYardsToMiles(String exp,String num){

        Assertions.assertEquals(exp,fromYardsToMiles(num));

    }
    @ParameterizedTest
    @CsvSource(value = {"0.3:0.1","0.03:0.01","0.003:0.001","3:1","0:0","-3:-1","27:9","30:10","33:11","57:19","60:20",
            "63:21","297:99","300:100", "303:101","2997:999","3000:1000", "3003:1001","29997:9999","30000:10000",
            "30003:10001", "299997:99999","300000:100000","300003:100001","2999997:999999","3000000:1000000",
            "3000003:1000001", "29999997:9999999", "Not a valid number.:qwerty","Not a valid number.:!",
            "Not a valid number.:+", "Not a valid number.:-", "Not a valid number.:йцукн"}, delimiter = ':')

    void test_fromYardsToFeet(String exp,String num){

        Assertions.assertEquals(exp,fromYardsToFeet(num));

    }



}
